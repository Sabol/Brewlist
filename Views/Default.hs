module Views.Default where

import Text.Blaze.Html5 ((!))
import qualified Web.Scotty as S
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

template body = do 
  H.docType
  H.head $ do
    H.script H.! A.src "https://unpkg.com/react@15.3.2/dist/react.js" $ do ""
    H.script H.! A.src "https://unpkg.com/react-dom@15.3.2/dist/react-dom.js" $ do ""
    H.script ! A.src "https://unpkg.com/babel-core@5.8.38/browser.min.js" $ do ""
  body 
