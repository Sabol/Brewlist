module Views.Home where

import qualified Web.Scotty as S
import Text.Blaze.Html5 
import Text.Blaze.Html5.Attributes
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
--import Text.Blaze.Html.Renderer.Text

template = do
  body $ do
    H.div ! A.id "facebookAvatar" $ do ""
    script ! type_ "text/babel" ! src "Avatar.js" $ do ""
    H.div ! A.id "angularFormTest" $ do ""
    script ! type_ "text/babel" ! src "LoginForm.js" $ do ""

      
