{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}

module Utils.DB where

import           Database.MongoDB
import qualified Data.Text.Lazy as T
import qualified Settings as S

-- Define our pipe
startConnection = do
  pipe <- connect $ Host S.dbIP (PortNumber S.dbPort)
  return pipe

run pipe act = access pipe master S.dbName act

