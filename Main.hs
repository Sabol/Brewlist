{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}

module Main where

--import           Data.Monoid ((<>))
import           Web.Scotty
import qualified Web.Scotty as S
import           Text.Blaze.Html.Renderer.Text
import           Network.Wai.Middleware.Static
import qualified Routes.BrewApi as B
import qualified Routes.Home as H
import qualified Data.Text.Lazy as T
import           Database.MongoDB
import           Control.Monad.Trans (liftIO)
import qualified Settings as S
import           Utils.DB

--　がんばってください。
--　むずかしですよ。

main :: IO ()
main = do
  -- Set up the db
  --pipe <- connect $ Host S.dbIP (PortNumber S.dbPort) 
  pipe <- startConnection
  scotty 3000 $ do
  -- Setup middleware to serve static files such as css, js and images
  middleware $ staticPolicy (noDots >-> addBase "static")

  -- Routing
  H.getHome "/"

  B.getBrew pipe

  B.putBrew pipe
