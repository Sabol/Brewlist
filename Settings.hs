module Settings where

import qualified Data.Text.Lazy as T
import           Database.MongoDB
import           Network.Socket.Internal
import           Network.Socket
--import           Network.Socket.HostName

dbName :: Database 
dbName = "brewDB"

dbPort :: PortNumber 
dbPort = 3030

dbIP :: HostName
dbIP = "127.0.0.1"
