{-# LANGUAGE RecordWildCards #-}

module Routes.Types where

import Data.Text
import Data.Aeson
--import Control.Applicative ((<$>), (<*>))

-- Let's derive some instances here for turning our type in to a from json
instance FromJSON Brew where
  parseJSON (Object o) = Brew 
                       <$> o .: "brewId"
                       <*> o .: "brewName"
                       <*> o .: "brewDesc"
                       <*> o .: "brewRating"
                       <*> o .: "ownerId"
                       <*> o .: "brewLoc"

instance ToJSON Brew where
  toJSON Brew {..} = object [ "brewId" .= brewId
                            , "brewName" .= brewName
                            , "brewDesc" .= brewDesc
                            , "brewRating" .= brewRating
                            , "ownerId" .= ownerId
                            , "brewLoc" .= brewLoc
                            ]

-- Export a constructor function for this to make sure rating is within bounds
data Brew = Brew { brewId :: Integer 
                 , brewName :: Text
                 , brewDesc :: Text
                 , brewRating :: Int
                 , ownerId :: Integer
                 , brewLoc :: Text
                 } deriving (Show)

-- Need to create a way to easily change JSON to BSON, etc.
-- A brew get's sent to the server without an ID and the database is 
-- allowed to generate it's id value.

-- This might not be necessary since it's likely that the end point
-- will no receive a Msg only raise them to the client
instance FromJSON Msg where
  parseJSON (Object o) = Msg
                       <$> o .: "msgId"
                       <*> o .: "msgLabel"
                       <*> o .: "msgText"

instance ToJSON Msg where
  toJSON Msg {..} = object [ "msgId" .= msgId
                           , "msgLabel" .= msgLabel
                           , "msgTest" .= msgText
                           ]

data Msg = Msg { msgId :: Integer
               , msgLabel :: Text
               , msgText :: Text
               } deriving (Show)
