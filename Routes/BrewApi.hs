{-# LANGUAGE OverloadedStrings #-}

module Routes.BrewApi where

import Web.Scotty
import qualified Data.Aeson as A
import Routes.Types
import Data.Monoid ((<>)) -- concat things that aren't strings
import Database.MongoDB
import qualified Utils.DB as DB

-- the routes look like this 
-- /brew/:Integer GET PUT
-- /brews GET

getBrew pipe = get "/brew/:id" $ do
             -- Does the id exist?
             -- idExists id
             -- we can check this later, let's get the json conversions working
             -- first.
             brewId <- param "id" 
             let msg = Msg { msgId = 1234556
                           , msgLabel = "Does not exist"
                           , msgText = ("The brew id: " <> brewId <> " not found!")
                           }
             json msg

-- this only echos the brew for now
putBrew pipe = put "/brew" $ do
             -- Here we would validate the json after parsing it and then stuff
             -- it into the database where the database is simply a colleciton of
             -- user objects with a list of their lists of brews and a list of
             -- individual brews as well, the reason for duplicating the data
             -- somewhat is so that we may access just a single brew some what
             -- easier and so that we can just grab a specific list when we want
             -- that.
             -- Parse out the brew json
             -- Do we perhaps need to create a BSON instance for this to quickly
             -- stuff it into the database?
             brew <- jsonData
             -- pop it into the database
             DB.run pipe (insert "brews" [])
             json (brew :: Brew) -- Type annotate here so ghc knows what instance of ToJSON to use

