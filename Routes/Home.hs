module Routes.Home where

import Data.Monoid ((<>))
import Web.Scotty
import qualified Views.Default
import qualified Views.Home
import qualified Utils.Blaze as B 

getHome route = get route $ do
                  B.blaze $ Views.Default.template Views.Home.template
