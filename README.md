### Brewlist

### It's a list... For beers that you have drank!

#### Written using Haskell for the server side and React for the client side
also MongoDB (I know, I know)

#### Why Haskell, you ask?
Because it's really cool, I wanted to learn it, and also the functional
programming paradigm and Haskell's super awesome type system lend it self to
writing more correct and less error prone programs which in turn lends it self
to writing more secure programs. (Even though the word secure is a bit
overloaded here, maybe consistent is better?)

#### Things I would like to do once the app is built
1. Switch over to using something like GHCJS for compile time checked js.
2. Implement a WebID module in Haskell to allow sign in/ up with that.
3. Perform a security audit of the app for fun.
